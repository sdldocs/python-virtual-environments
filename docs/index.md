# Python Virtual Environments

목차

- [Virtual Environments가 필요한 이유?](primer/#why-the-need-for-virtual-environments)
- [Virtual Environments 란?](primer/#what-is-a-virtual-environments)
- [Virtual Environments 사용](primer/#using-virtual-environments)
- [Virtual Environments의 동작](primer/#how-does-a-virtual-environments-work)
- [virtualenvwrapper로 Virtual Environments 관리](primer/#managing-virtual-environmants-with-virtualenv-wrapper)
- [Python 여러 다른 버전의 사용](primer/#using-different-versions-of-python)
- [맺으며](primer/#conclusion)


이 페이지에서는 Python 프로젝트를 위해 [가상 환경(Virtual Environments)](https://docs.python.org/3/library/venv.html#venv-def)을 사용하여 각각 다른 버전의 Python을 실행할 수 있는 별도의 환경을 만들고 관리하는 방법에 대해 설명하고 있다. 또한 Python 종속성이 어떻게 저장되고 해결되는지 기술하고 있다.

- 2018-01-12 업데이트: Python 3.6+에서의 pyenv과 venv 명확한 사용 
- 2016-06-11 업데이트: virtualenv에서 Python 버전 변경 섹션 추가
